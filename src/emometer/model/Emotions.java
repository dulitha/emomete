/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package emometer.model;

import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Duli-chan
 */
public class Emotions {

    public static final int EXCITED = 0;
    public static final int ANGRY = 1;
    public static final int SAD = 2;
    public static final int APATHETIC = 3;
    public static final int HAPPY = 4;
    public static final int MOTIVATED = 5;
    public static final int DEPRESSED = 6;
    public static final int CALM = 7;
    public static final int NOCONTROL = 8;
    public static ArrayList<String> names;
    public static ArrayList<Integer> values;

    static {
        Emotions.names = new ArrayList<String>();
        Emotions.values = new ArrayList<Integer>();
        Class emotion = Emotions.class;
        Field[] fields = emotion.getFields();
        for (int i = 0; i < fields.length; i++) {
            Class<?> type = fields[i].getType();
            if (type == int.class) {
                String name = fields[i].getName();
                Emotions.names.add(name);
                try {
                    Emotions.values.add(fields[i].getInt(new Emotions()));
                } catch (IllegalArgumentException ex) {
                    Logger.getLogger(Emotions.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IllegalAccessException ex) {
                    Logger.getLogger(Emotions.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        }
    }
    private int emotion;
    private String note;
    private Date currentday;

    public Emotions() {
        currentday = new Date();
    }

    /**
     * @return the emotion
     */
    public int getEmotion() {
        return emotion;
    }

    /**
     * @param emotion the emotion to set
     */
    public void setEmotion(int emotion) {
        this.emotion = emotion;
    }

    /**
     * @return the note
     */
    public String getNote() {
        return note;
    }

    /**
     * @param note the note to set
     */
    public void setNote(String note) {
        this.note = note;
    }

    /**
     * @return the currentday
     */
    public Date getCurrentday() {
        return currentday;
    }

    /**
     * @param currentday the currentday to set
     */
    public void setCurrentday(Date currentday) {
        this.currentday = currentday;
    }
}
