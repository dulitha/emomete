package emometer.utility;

import java.awt.Dimension;
import javax.swing.GroupLayout;
import javax.swing.JDialog;
import javax.swing.JPanel;
/**
 * @author Duli-chan
 */
public class Window {
    // panels loading method

    public static void switchPanel(JPanel panelsLoadPanel, JPanel newPanel) {
        panelsLoadPanel.setSize(panelsLoadPanel.getWidth(), panelsLoadPanel.getHeight());
        panelsLoadPanel.removeAll();
        GroupLayout l = new GroupLayout(panelsLoadPanel);
        panelsLoadPanel.setLayout(l);
        l.setHorizontalGroup(l.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(newPanel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE));
        l.setVerticalGroup(l.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(newPanel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE));
        System.gc();
    }
/**
     * 
     * @param newPanel= The Panel we want to appear now
     * Remember to edit the first argument to the SwitchPanel method
     */
    public static void switchPanel(JPanel newPanel) {
        switchPanel(emometer.view.MainFrame.getPanel(), newPanel);
    }
/**
     * 
     * @param newPanel = The Panel we want to appear now
     * Remember to edit the first argument to the SwitchPanel method
     */
//    public static void switchMasterDialog(JPanel newPanel) {
// 
//        Dimension preferredSize = newPanel.getPreferredSize();
//        preferredSize.setSize(preferredSize.getWidth()+20, preferredSize.getHeight()+20);
//        MasterJDialog.getInstance().setSize(preferredSize);
//        switchPanel(MasterJDialog.getPanel(), newPanel);
//        MasterJDialog.getInstance().setVisible(true);
//    }
}
