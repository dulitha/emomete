/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package emometer.controller;

import emometer.model.Emotions;
import java.sql.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import redis.clients.jedis.Jedis;

/**
 *
 * @author Duli-chan
 */
public class DataHandler {

    public static void saveEmotion(Emotions emotion) {
        Jedis jedis = emometer.EmoMeter.getPool().getResource();
        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put("emotion", Integer.toString(emotion.getEmotion()));
        hashMap.put("note", emotion.getNote());
        hashMap.put("day", Long.valueOf(emotion.getCurrentday().getTime()).toString());
        try {
            Long incr = jedis.incr("ids:emotion");
            jedis.hmset("emotion:"+incr, hashMap);
        }catch(Exception e){
            e.printStackTrace();
        } finally {
            emometer.EmoMeter.getPool().returnResource(jedis);
        }

    }

    public static HashSet<Emotions> getEmotion() {
        Jedis jedis = emometer.EmoMeter.getPool().getResource();
        HashSet<Emotions> set= new HashSet<Emotions>();
        try {
            int id = Integer.parseInt(jedis.get("ids:emotion"));
            System.out.println(id);
            for (int i = 1; i <= id; i++) {
                Emotions emotions = new Emotions();
                Map<String, String> emotion = jedis.hgetAll("emotion:"+i);
                emotions.setEmotion(Integer.parseInt(emotion.get("emotion").toString()));
                emotions.setCurrentday(new Date(Long.parseLong(emotion.get("day").toString())));
                emotions.setNote(emotion.get("note").toString());
                set.add(emotions);
            }
            
        }catch(Exception e){
            e.printStackTrace();
        }
        finally {
            emometer.EmoMeter.getPool().returnResource(jedis);
        }
        return set;
    }

    public static void cleanDB() {
        Jedis jedis = emometer.EmoMeter.getPool().getResource();
        try {
            int id = Integer.parseInt(jedis.get("ids:emotion"));
            for (int i = 1; i <= id; i++) {
                jedis.del("emotion:"+i);
            }
            jedis.del("ids:emotion");
        } catch (Exception e) {
            e.printStackTrace();
        }finally{
            emometer.EmoMeter.getPool().returnResource(jedis);
        }
    }
}
