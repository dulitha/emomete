/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package emometer;

import emometer.utility.Window;
import emometer.view.Home;
import emometer.view.MainFrame;
import javax.swing.UIManager;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 *
 * @author Duli-chan
 */
public class EmoMeter {

    private static JedisPool pool;
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            e.printStackTrace();
        }
        pool = new JedisPool(new JedisPoolConfig(), "localhost");
        MainFrame mf = MainFrame.getInstance();
        mf.setVisible(true);
        Window.switchPanel(new Home());
    }

    /**
     * @return the pool
     */
    public static JedisPool getPool() {
        return pool;
    }
}
